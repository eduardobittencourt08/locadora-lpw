<?php

  $dbuser = '';
  $dbpass = '';
  $pdo = new PDO('mysql:host=localhost;dbname=locadora', $dbuser, $dbpass);
  
  switch($_REQUEST['action']){
    case 'insertClient':
      insertClient($pdo);
      break;
    case 'listClients':
      listClients($pdo);
      break;
    case 'getClient':
      getClient($pdo);
      break;
    case 'deleteClient':
      deleteClient($pdo);
      break;
    case 'insertCar':
      insertCar($pdo);
      break;
    case 'listCars':
      listCars($pdo);
      break;
    case 'getCar':
      getCar($pdo);
      break;
    case 'deleteCar':
      deleteCar($pdo);
      break;
    case 'rentCar':
      rentCar($pdo);
      break;
    case 'returnCar':
      returnCar($pdo);
      break;
  }
  
  function insertClient($pdo) {
    if($_REQUEST['id'] == null){
      $insertClient = $pdo->prepare('INSERT INTO usuarios(nome, cpf, telefone) VALUES(?, ?, ?)');
      $insertClient->execute(array($_REQUEST['nome'], $_REQUEST['cpf'], $_REQUEST['telefone']));
    } else {
      $insertClient = $pdo->prepare('UPDATE usuarios SET nome=?, cpf=?, telefone=? WHERE id=?');
      $insertClient->execute(array($_REQUEST['nome'], $_REQUEST['cpf'], $_REQUEST['telefone'], $_REQUEST['id']));
    }
  }
  
  function listClients($pdo) {
    $listClients = $pdo->prepare('SELECT * FROM usuarios');
    $listClients->execute();
    echo json_encode($listClients->fetchAll());
  }

  function getClient($pdo) {
    $listClients = $pdo->prepare('SELECT * FROM usuarios WHERE id=?');
    $listClients->execute(array($_REQUEST['id']));
    echo json_encode($listClients->fetchAll());
  }

  function deleteClient($pdo) {
    $deleteClient = $pdo->prepare('DELETE FROM usuarios WHERE id=?');
    $deleteClient->execute(array($_REQUEST['id']));
  }

  function insertCar($pdo) {
    if($_REQUEST['id'] == null){
      $insertCar = $pdo->prepare('INSERT INTO carros(marca, modelo, ano, cor) VALUES(?, ?, ?, ?)');
      $insertCar->execute(array($_REQUEST['marca'], $_REQUEST['modelo'], $_REQUEST['ano'], $_REQUEST['cor']));
    } else {
      $insertCar = $pdo->prepare('UPDATE carros SET marca=?, modelo=?, ano=?, cor=? WHERE id=?');
      $insertCar->execute(array($_REQUEST['marca'], $_REQUEST['modelo'], $_REQUEST['ano'], $_REQUEST['cor'], $_REQUEST['id']));
    }
  }
  
  function listCars($pdo) {
    $listCars = $pdo->prepare('SELECT * FROM carros');
    $listCars->execute();
    echo json_encode($listCars->fetchAll());
  }

  function getCar($pdo) {
    $listCars = $pdo->prepare('SELECT * FROM carros WHERE id=?');
    $listCars->execute(array($_REQUEST['id']));
    echo json_encode($listCars->fetchAll());
  }

  function deleteCar($pdo) {
    $deleteCar = $pdo->prepare('DELETE FROM carros WHERE id=?');
    $deleteCar->execute(array($_REQUEST['id']));
  }

  function rentCar($pdo) {
    $updateUsado = $pdo->prepare('UPDATE carros SET utilizado = ? where id = ?');
    $updateUsado->execute(array($_REQUEST['userId'], $_REQUEST['carId']));
  }

  function returnCar($pdo) {
    $updateUsado = $pdo->prepare('UPDATE carros SET utilizado = null where id = ?');
    $updateUsado->execute(array($_REQUEST['carId']));
  }

?>