# 🚗 Projeto Locadora 🚗

Projeto com finalidade de demonstrar alguns conhecimentos em PHP e JS para a cadeira de Linguagem de Programação para Web.

## 📂 Estrutura do Projeto

O projeto conta com uma estrutura de arquivos simples composta por:

- 📄 4 arquivos `HTML` contendo as páginas de início, cadastro de carros, cadastro de usuários e aluguel de veículos
- 🐘 Arquivo `PHP` com as funções de controle e conexão com o banco de dados (MySQL)
- 🐬 Arquivo `SQL` contendo as informações da criação das tabelas do banco de dados
- ❗️  Arquivo `MD` com documentação do projeto

## ⚙️ Como Executar

Para executar o projeto, algumas configurações devem ser feitas:

- ⬆️ Criar o banco de dados com o nome `locadora` e importar o arquivo `SQL`
- 📢 Informar dentro do arquivo `functions.php` o usuário e senha do banco de dados
- 🏃 Rodar na raiz do projeto o comando `php -S localhost:8000`